# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

FROM python:3.9-alpine

COPY . /src

WORKDIR /src

ENV K8S_VERSION 1.23.5
ENV KOPS_VERSION 1.23.0

RUN apk add --no-cache zsh bash openssl openssh libffi ca-certificates make ansible sed \
  && apk add --no-cache --virtual build-dependencies gcc musl-dev openssl-dev libffi-dev curl rust cargo \
  && sed -i -e 's#/root:/bin/ash#/root:/bin/zsh#' /etc/passwd \
  && curl -sSL -o /tmp/install-poetry.py https://install.python-poetry.org \
  && python /tmp/install-poetry.py \
  && export PATH="/root/.local/bin:$PATH" \
  && poetry config virtualenvs.create false \
  && poetry install --no-dev --no-interaction --no-ansi \
  && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash \
  && curl -sL https://dl.k8s.io/v${K8S_VERSION}/kubernetes-client-linux-amd64.tar.gz | tar x -z -C /tmp && mv /tmp/kubernetes/client/bin/kubectl /usr/local/bin/kubectl \
  && curl -o /usr/local/bin/kops -sL https://github.com/kubernetes/kops/releases/download/v${KOPS_VERSION}/kops-linux-amd64 \
  && chmod 755 /usr/local/bin/kops \
  && kubectl completion zsh > /usr/share/zsh/site-functions/_kubectl  \
  && helm completion zsh > /usr/share/zsh/site-functions/_helm \
  && kops completion zsh > /etc/zsh/kops \
  && poetry completions zsh > /usr/share/zsh/site-functions/_poetry \
  && cp share/completion/zsh/_gitlabci /usr/share/zsh/site-functions/_gitlabci \
  && cp share/zshrc /root/.zshrc \
  && python /tmp/install-poetry.py --uninstall && rm /tmp/install-poetry.py \
  && apk del build-dependencies

EXPOSE 8001/tcp

ENV PATH /src:/root/.local/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV SHELL /bin/zsh

ENTRYPOINT ["/bin/zsh"]
