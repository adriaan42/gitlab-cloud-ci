# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

VERSION = v$(shell grep "^version" pyproject.toml | cut -d= -f2 | tr -d '"[:space:]')
DOCKER_IMAGE = registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci
DOCKER ?= docker
PWD = $(shell pwd)
CLUSTER_NAME=$(shell grep '"cluster_name"' config.json | tr -d '",[:space:]' | cut -d: -f2)

AWS_DEFAULT_REGION ?=
AWS_ACCESS_KEY_ID ?=
AWS_SECRET_ACCESS_KEY ?=
AWS_SESSION_TOKEN ?=
https_proxy ?=
http_proxy ?=
no_proxy ?=

default: format lint test

format:
	.ci/format.sh

lint:
	.ci/lint.sh
	.ci/license_check.sh

test:
	python3 -m pytest tests

docker: clean
	$(DOCKER) build --pull --no-cache -t "$(DOCKER_IMAGE):$(VERSION)" .

gitlab-runner:
	make -C ./hack/gitlab-runner build

shell:
	@EXTRA_ARGS=""
	[ ! -f .local.env ] || EXTRA_ARGS="$$EXTRA_ARGS --env-file=$(PWD)/.local.env"
	[ -z "$(AWS_DEFAULT_REGION)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env AWS_DEFAULT_REGION"
	[ -z "$(AWS_ACCESS_KEY_ID)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env AWS_ACCESS_KEY_ID"
	[ -z "$(AWS_SECRET_ACCESS_KEY)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env AWS_SECRET_ACCESS_KEY"
	[ -z "$(AWS_SESSION_TOKEN)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env AWS_SESSION_TOKEN"
	[ -z "$(https_proxy)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env https_proxy"
	[ -z "$(http_proxy)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env http_proxy"
	[ -z "$(no_proxy)" ] || EXTRA_ARGS="$$EXTRA_ARGS --env no_proxy"
	$(DOCKER) run --rm \
		--volume $(PWD):/src \
		--volume $(PWD)/share/zshrc:/root/.zshrc:ro \
		--workdir=/src \
		--env HELM_TLS_ENABLE=true \
		--env CLUSTER_NAME=$(CLUSTER_NAME) \
		--env "KOPS_STATE_STORE=s3://$(CLUSTER_NAME)-kops-state-store" \
		--publish 3030:3030 \
		--publish 8001:8001 \
		$$EXTRA_ARGS \
		-it $(DOCKER_IMAGE):$(VERSION)

clean:
	rm -rf .pytest_cache
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

.PHONY: format lint test docker shell clean gitlab-runner
