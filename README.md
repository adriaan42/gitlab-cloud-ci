# Gitlab Custom Cloud CI

This project creates a Kubernetes-based CI-infrastructure for Gitlab from scratch.
Clusters can run on either AWS or on a Linux host (ssh root access required).

**Highlights**

* Allow running **privileged containers** on highly performant machines for CI pipelines
* Scale horizontally: run an (almost) arbitrary number of CI jobs in parallel
* Scale dynamically: scale up and down based on the current workload situation

## Table of Contents

* [Getting Started](#getting-started)
* [On-Premise Setup](#on-premise-setup)
* [Kubernetes Dashboard](#kubernetes-dashboard)
* [Monitoring](#monitoring)
* [SSH Access](#ssh-access)
* [References](#references)

## Getting Started

1. `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are needed to create the necessary AWS resources.
  The following permissions must be associated with the API keys:
    * `AmazonEC2FullAccess`
    * `AmazonRoute53FullAccess`
    * `AmazonS3FullAccess`
    * `IAMFullAccess`
    * `AmazonVPCFullAccess`
    * `AmazonSQSFullAccess`
    * `AmazonEventBridgeFullAccess`

See [github.com/kubernetes/kops/blob/master/docs/getting_started/aws.md](https://github.com/kubernetes/kops/blob/master/docs/getting_started/aws.md)  for detailed instructions.

```bash
$ export AWS_ACCESS_KEY_ID=...
$ export AWS_SECRET_ACCESS_KEY=...
```

2. Create `config.json`:

```bash
$ cp config.json.sample config.json
# now edit config.json
```

3. Launch Docker container:

```bash
$ make shell
```

4. Create cluster on AWS:

```bash
$ gitlabci create aws

# optionally, deploy AWS autoscaler add-on:
$ gitlabci autoscaler deploy
```

5. Deploy Gitlab runner:

Go to your projects CI setup page (Settings -> CI/CD -> Runners) and grab the token for Runner registration.

Then, for each runner which you want to deploy, create a local copy of `share/k8s/gitlab-runner/values.yaml`,
adjust the configuration and deploy the runner, e.g.

```bash
$ cp share/k8s/gitlab-runner/values.yaml share/k8s/gitlab-runner/example-runner.yaml
$ gitlabci runner deploy \
    --config=share/k8s/gitlab-runner/example-runner.yaml \
    example-runner
```

Hint: You can "Disable Shared Runners" in the Gitlab UI to test your custom runner.

To purge a Gitlab runner from your cluster, run

```bash
$ gitlabci runner purge example-runner
```

Hint: Use `gitlabci runner list` to get an overview of all deployed runners.

6. To tear down the cluster and all resources associated with it (don't do this if you want to keep your cluster):

```bash
$ gitlabci destroy aws
```

## On-Premise Setup

It is also possible to deploy the Kubernetes cluster on-premise (technically, SSH-based) instead of AWS.

**Prerequisites**

- The SSH keys used by `gitlabci` need to be in PEM format.
  You can convert keys with: `ssh-keygen -p -m PEM -f <private_key>` or generate a new one with `ssh-keygen -t rsa -m PEM`.
  The file `~/.ssh/config` is partially supported to determine SSH connection settings.
- Ensure iptables tooling does not use the nftables backend: See [legacy iptables](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#ensure-iptables-tooling-does-not-use-the-nftables-backend).
- [docker-ce](https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker)
- [kubeadm, kubectl, kubelet](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-runtime)
- Swap must be deactivated (`swapoff -a`)

**Setup**

```bash
$ gitlabci create ssh DESTINATION
```

**Tear Down**

```bash
$ gitlabci destroy ssh DESTINATION
```

To test the on-premise setup you can use the provided Vagrantfile in `./test/siemens/onpremise/`.
See [README.vagrant.md](./tests/siemens/onpremise-vm/README.vagrant.md) for further instructions. 

## Kubernetes Dashboard

A Kubernetes dashboard can be deployed and accessed as follows:

```bash
$ gitlabci dashboard deploy
$ kubectl proxy --address=0.0.0.0 &
$ gitlabci dashboard get-token
```

Now open
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:https/proxy/
in your browser and use the above token to login.

## Monitoring

Prometheus together with Grafana can be deployed for in-depth monitoring as follows:

```bash
$ gitlabci monitoring deploy
```

This will increase the Memory requirements of the master by around 500MB.

You can access a dashboard as follows:

```bash
$ export POD_NAME=$(kubectl get pods --namespace monitoring -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")
$ kubectl port-forward --address 0.0.0.0 -n monitoring ${POD_NAME} 3030:3000 &
$ gitlabci monitoring get-admin-password
```

Now open [http://localhost:3030](http://localhost:3030) in your browser and login as 'admin' with the password generated
above. The [default dashboard](https://grafana.com/grafana/dashboards/6417) can be found at
[http://localhost:3030/dashboards](http://localhost:3030/dashboards).

Recommended dashboards:

* [Gitlab Runner Metrics](https://grafana.com/grafana/dashboards/14016)

## SSH Access

```bash
$ kubectl get nodes -o wide
$ ssh -i <your private ssh key> ubuntu@<external ip>
```

# Maintenance

See [MAINTENANCE.md](./MAINTENANCE.md).

## References

* https://docs.gitlab.com/runner/executors/kubernetes.html
* https://docs.gitlab.com/runner/install/kubernetes.html
* https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
* https://github.com/kubernetes/autoscaler
