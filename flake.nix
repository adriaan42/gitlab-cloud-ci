# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

{
  description = "A flake using poetry2nix";

  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.utils.url = "github:numtide/flake-utils";
  inputs.poetry2nix-src.url = "github:nix-community/poetry2nix";

  outputs = { nixpkgs, utils, poetry2nix-src, self }: utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let

      pkgs = import nixpkgs { inherit system; overlays = [ poetry2nix-src.overlay ]; };

      myAppEnv = pkgs.poetry2nix.mkPoetryEnv {
        projectDir = ./.;
      };
      myApp = pkgs.poetry2nix.mkPoetryApplication
        {
          projectDir = ./.;
        };

    in
    {
      packages = {
        gitlab-cloud-ci = myApp;
      };

      devShell = pkgs.mkShell
        {
          buildInputs = [
            myAppEnv
            # pkgs.python3Packages.python-language-server
          ];
        };

      defaultPackage = myApp;
    });

}
