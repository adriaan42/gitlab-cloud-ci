This fork of gitlab-runner contains a workaround for [gitlab-runner/issues/4125](https://gitlab.com/gitlab-org/gitlab-runner/issues/4125).

See `./patches`.

It should be backwards compatible with the upstream gitlab-runner.

## Updating

1. Adjust the revs in `config.mk`.
2. `make build`
3. `make push-tagged`
4. `make push-latest`
