# Copyright (c) Siemens AG, 2019 - 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
DOCKER_IMAGE_RUNNER := registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci/gitlab-runner
DOCKER_IMAGE_HELPER := registry.gitlab.com/cip-project/cip-testing/gitlab-cloud-ci/gitlab-runner-helper

GITLAB_RUNNER_VERSION := v14.4.0

# head over to https://gitlab.com/gitlab-org/gitlab-runner/-/tree/v13.12.0 and write down the rev here
# gitlab-runner and gitlab-runner-helper use an internal API, so ideally they are built from the same src rev,
# but this does not always seem to be the case; try to match it as close as possible (e.g. bleeding)
#
# https://hub.docker.com/r/gitlab/gitlab-runner-helper/tags?page=1&name=x86&ordering=last_updated
HELPER_IMAGE_REV := ubuntu-x86_64-v14.4.0
