#!/usr/bin/env bash
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -euo pipefail
set -x

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

CLUSTER_TYPE=${1:-}
[ -n "$CLUSTER_TYPE" ] || {
    echo "Please specify CLUSTER_TYPE. Possible values: small or large"
    exit 1
}

if gitlabci status >/dev/null 2>&1; then
    echo "Error! A cluster with that name already exists"
fi

gitlabci create aws
gitlabci hardening deploy
gitlabci autoscaler deploy
gitlabci dashboard deploy

"$SCRIPT_DIR"/deploy_runners.sh "$CLUSTER_TYPE"
