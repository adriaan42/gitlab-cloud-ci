#!/usr/bin/env bash
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
set -euo pipefail
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

CLUSTER_TYPE=${1:-}
[ -n "$CLUSTER_TYPE" ] || {
    echo "Please specify CLUSTER_TYPE. Possible values: small or large"
    exit 1
}

EPOCH=$(date "+%s")

# dots are not allowed in the runner name
RUNNER_NAME="$CLUSTER_NAME-playground-$EPOCH"
RUNNER_NAME=${RUNNER_NAME//./-}
gitlabci runner deploy "--token=$TOKEN_CIP_PLAYGROUND" "--config=$SCRIPT_DIR/$CLUSTER_TYPE/runner.yaml" "$RUNNER_NAME"

RUNNER_NAME="$CLUSTER_NAME-project-$EPOCH"
RUNNER_NAME=${RUNNER_NAME//./-}
gitlabci runner deploy "--token=$TOKEN_CIP_PROJECT" "--config=$SCRIPT_DIR/$CLUSTER_TYPE/runner.yaml" "$RUNNER_NAME"
