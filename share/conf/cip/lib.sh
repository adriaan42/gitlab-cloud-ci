#!/usr/bin/env bash
###############################################################################
# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
###############################################################################
ask_confirm() {
    if [ ! "$FORCE" = "true" ]; then
        while true; do
            read -p "Command: $*
Continue (y/n)? " choice
            case "$choice" in
                y | Y) break ;;
                n | N) return 0 ;;
                *) echo "invalid" ;;
            esac
        done
    fi
    "${@}"
}
