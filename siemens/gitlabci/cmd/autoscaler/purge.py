# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

import click

from siemens.gitlabci.cli import autoscaler as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import helm
    from siemens.gitlabci.lib.aws import delete_autoscaler_iam

log = logging.getLogger(__name__)


@cli.command()
@click.pass_context
def purge(ctx):
    """Purge AWS autoscaler pod"""

    cfg = ctx.obj["cfg"]
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]
    asg_policy_name = cfg.aws["asg_policy_name"]

    log.info("Purging deployment cluster-autoscaler")
    helm("uninstall", "aws-auto-scaler", "--namespace=kube-system")
    delete_autoscaler_iam(cluster_name, region, asg_policy_name)
