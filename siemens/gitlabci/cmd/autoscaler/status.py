# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

from siemens.gitlabci.cli import autoscaler as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import helm

log = logging.getLogger(__name__)


@cli.command()
def status():
    """Get status of cluster-autoscaler"""
    log.info("Getting status of cluster-autoscaler")
    print(helm("status", "aws-auto-scaler", "--namespace=kube-system"))
