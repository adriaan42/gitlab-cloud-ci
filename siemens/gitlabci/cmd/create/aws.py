# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os
import tempfile
import time

import click
import yaml

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from siemens.gitlabci.cli import create as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    import sys

    import boto3
    from siemens.gitlabci.lib.kops import kops
    from siemens.gitlabci.lib.aws import s3_exists_bucket
    from siemens.gitlabci.lib.kubernetes import (
        configure_kubernetes_client,
        add_dedicated_role_to_master,
        tolerate_deployment_on_master,
        is_cluster_up,
    )

log = logging.getLogger(__name__)


@cli.command()
@click.pass_context
@click.option(
    "--node-count",
    default=1,
    type=int,
    help="Initial K8s node count (>=1)",
    show_default=True,
)
@click.option(
    "--kubernetes-version",
    default="v1.23.5",
    help="Specify K8s version",
    show_default=True,
)
@click.option(
    "--enable-metrics",
    default=False,
    type=bool,
    help="Deploy metrics server",
    show_default=True,
)
def aws(ctx, node_count, kubernetes_version, enable_metrics):
    """Create ISAR CI setup running on AWS"""
    cfg = ctx.obj["cfg"]
    cluster_name = cfg.aws["cluster_name"]
    region = cfg.aws["region"]
    ssh_pub_key = os.path.expanduser(cfg.ssh_pub_key)

    master_zones = cfg.aws["master_zones"]
    master_size = cfg.aws["master_size"]
    node_zones = cfg.aws["node_zones"]
    node_size = cfg.aws["node_size"]

    s3 = boto3.resource("s3", region_name=region)
    bucket_name = "{}-kops-state-store".format(cluster_name)
    state_store = "s3://{}".format(bucket_name)

    if not s3_exists_bucket(s3, bucket_name):
        log.info("Creating s3 bucket %s in region %s", bucket_name, region)
        s3.create_bucket(
            Bucket=bucket_name, CreateBucketConfiguration={"LocationConstraint": region}
        )
        log.info("Waiting for bucket creation to finish")
        s3.Bucket(bucket_name).wait_until_exists()
        log.info("Enabling bucket versioning")
        bucket_versioning = s3.BucketVersioning(bucket_name)
        bucket_versioning.enable()

    full_cluster_name = "{}.k8s.local".format(cluster_name)
    if not is_cluster_up(node_count):
        log.info("Creating K8s master and nodes on AWS")
        kops_args = [
            "create",
            "cluster",
            "--stderrthreshold",
            "0",
            "--state",
            state_store,
            "--kubernetes-version",
            kubernetes_version,
            "--networking=calico",
            "--zones",
            node_zones,
            "--master-zones",
            master_zones,
            "--master-size",
            master_size,
            "--node-count",
            max(1, node_count),  # 0 is not possible with kops
            "--node-size",
            node_size,
            "--authorization=RBAC",
            "--cloud=aws",
            "--ssh-public-key",
            ssh_pub_key,
            "--name",
            full_cluster_name,
            "--dry-run",
            "-oyaml",
        ]
        vpc_id = cfg.aws.get("vpc_id", None)
        subnets = cfg.aws.get("subnets", None)
        ami_id = cfg.aws.get("ami", None)
        public_ip = cfg.aws.get("public_ip", None)
        if vpc_id:
            kops_args.extend(["--vpc", vpc_id])
        if subnets:
            kops_args.extend(["--subnets", subnets])
        if ami_id:
            kops_args.extend(["--image", ami_id])
        if public_ip is not None:
            if public_ip:
                kops_args.append("--associate-public-ip=true")
            else:
                if not vpc_id:
                    raise 'Private IPs need a VPC to be specified with \
                        the value "aws.vpc_id" in the "config.json" file.'
                if not subnets:
                    raise 'Private IPs need a subnet to be specified with \
                        the value "aws.subnets" in the "config.json" file.'
                kops_args.append("--associate-public-ip=false")
                kops_args.append("--api-loadbalancer-type=internal")
                kops_args.append("--topology=private")
                kops_args.extend(["--utility-subnets", subnets])
        cmd = kops(*kops_args)

        yaml_docs = [
            yaml.load(x, Loader=Loader)
            for x in cmd.stdout.decode("utf-8").split("---")
            if x.strip() != ""
        ]
        if enable_metrics:
            found_metrics = False
            for item in yaml_docs:
                if item["kind"] == "Cluster":
                    # this is currently broken in kops 1.23.0
                    item["spec"]["metricsServer"] = {"enabled": True, "insecure": True}
                    found_metrics = True
                    break
            if not found_metrics:
                raise RuntimeError("Cluster item not found")

        fname = None
        with tempfile.NamedTemporaryFile(mode="w", delete=False) as f:
            fname = f.name
            for item in yaml_docs:
                f.write("---\n")
                yaml.dump(item, stream=f, Dumper=Dumper)

        log.debug("Creating cluster using file %s", fname)
        # this does not create the cluster yet
        kops("create", "-f", fname)
        os.remove(fname)

        kops(
            "create",
            "secret",
            "--state",
            state_store,
            "--name",
            full_cluster_name,
            "sshpublickey",
            "admin",
            "-i",
            ssh_pub_key,
        )

        # this will create resources in aws
        kops("update", "cluster", full_cluster_name, "--yes", _out=sys.stdout)

        log.info(
            "Waiting for cluster to finish initialization. "
            "This takes about 5 minutes."
        )
        for i in range(0, 30):
            try:
                kops("export", "kubecfg", "--admin")
                break
            except Exception:
                time.sleep(10)
        kops("validate", "cluster", "--wait=10m", _out=sys.stdout)

    configure_kubernetes_client()
    deployments = [
        "calico-kube-controllers",
        "coredns",
        "coredns-autoscaler",
        "dns-controller",
    ]
    if enable_metrics:
        deployments.append("metrics-server")
    for dname in deployments:
        tolerate_deployment_on_master(dname, "kube-system")

    add_dedicated_role_to_master()

    kops("validate", "cluster", "--wait=5m", _out=sys.stdout)
    log.info("Cluster is up and running.")
