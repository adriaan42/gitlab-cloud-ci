# On-Premise Test VM

This directory contains the necessary files to setup a test VM to test the on-premise commands.

## Setup

Install Vagrant and Virtualbox.

### SSH Configuration

To use the VM with `gitlabci` add the following configuration to `./secrets/ssh/config` in this repository. 
```
Host vagrant
  HostName 192.168.50.10
  User vagrant
  Port 22
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile=~/.ssh/vagrant_id_rsa
  IdentitiesOnly yes
  LogLevel FATAL
  ForwardAgent yes
```
Generate `./secrets/ssh/vagrant_id_rsa` with:

```bash
$ ssh-keygen -t rsa -b 2048 -m PEM
```

Start the Docker environment with:

```bash
$ make shell
```

Change owner and permissions of `/root/.ssh/config` and `/root/.ssh/vagrant_id_rsa` with:

```bash
$ chmod 400 ~/.ssh/vagrant_id_rsa
$ chmod 600 ~/.ssh/config
$ chown $USER:$USER ~/.ssh/config
$ chown $USER:$USER ~/.ssh/vagrant_id_rsa
```

## Starting the VM

```bash
$ VAGRANT_SSH_PUB_KEY=../../../secrets/ssh/vagrant_id_rsa.pub vagrant up
```
